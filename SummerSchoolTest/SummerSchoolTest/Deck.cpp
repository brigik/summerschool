#include "Deck.h"
#include "Card.h"
#include <iostream>
#include <random>


Deck::Deck()
{
}


Deck::~Deck()
{
}

size_t Deck::m_numberOfCardsInDeck = 0;

void Deck::shuffleDeck()
{
	std::cout << m_cardArray.size() << std::endl;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, m_numberOfCardsInDeck - 1);

	for (auto i = 0; i < m_numberOfCardsInDeck; ++i)
	{
		swap(m_cardArray.at(i), m_cardArray.at(dis(gen)));
	}
}

void Deck::swap(Card & firstCard, Card & secondCard)
{
	Card auxiliary = firstCard;
	firstCard = secondCard;
	secondCard = auxiliary;
}

void Deck::printDeck() const
{
	for (const auto& card : m_cardArray)
	{
		card.printCard();
		std::cout << std::endl;
	}
}

size_t Deck::getNumberOfCards()
{
	return m_numberOfCardsInDeck;
}

Card Deck::dealCard()
{
	return m_cardArray.at(m_index++);
}
