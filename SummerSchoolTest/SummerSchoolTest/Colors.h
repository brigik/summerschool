#pragma once
#include<iostream>
class Colors
{
	int m_red;
	int m_green;
	int m_blue;

public:
	Colors();

	~Colors();

	Colors(const int& red, const int& green, const int& blue);

	void setRed(const int& red);

	void setGreen(const int& green);

	void setBlue(const int& blue);

	friend std::ostream& operator << (std::ostream& o, const Colors& color);

};

