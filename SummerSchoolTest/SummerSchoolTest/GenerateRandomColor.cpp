#include "Colors.h"
#include<iostream>
#include<random>
#include <cstdlib>
#include <ctime>
using namespace std;

int generateRandomNumber()
{
	int randomNumber;
	srand(time(NULL));
	randomNumber = rand() % 255 + 1;
	return randomNumber;
}

Colors generateRandomColor()
{
	Colors *color = new Colors(generateRandomNumber(), generateRandomNumber(), generateRandomNumber());
	return *color;
}

int main()
{
	cout << generateRandomColor();
	return 0;
}