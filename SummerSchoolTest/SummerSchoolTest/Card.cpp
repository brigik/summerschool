#include "Card.h"
#include <iostream>
#include <string>



Card::Card()
{
}

Card::Card(const Number & number, const Symbol & symbol, const Shading & shading, const Color & color) :
	m_number(number),
	m_symbol(symbol),
	m_shading(shading),
	m_color(color)
{
}

Card::Card(const Card & other) :
	Card(other.m_number, other.m_symbol, other.m_shading, other.m_color)
{
}

std::string Card::numberToString(const Number & number)
{
	int currentCardNumber = static_cast<int>(number);
	return std::to_string(currentCardNumber);
}

void Card::printCard() const
{
	std::cout << numberToString(m_number) << " " << static_cast<char>(m_symbol) << " " << static_cast<char>(m_shading) << " " << static_cast<char>(m_color);
}


Card::~Card()
{
}

std::ostream & operator<<(std::ostream & o, const Card & card)
{
	o << card.numberToString(card.m_number) << " " << static_cast<char>(card.m_symbol) << static_cast<char>(card.m_shading) << static_cast<char>(card.m_color);
	return o;
}
