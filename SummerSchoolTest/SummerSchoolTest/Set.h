#pragma once
#include "Card.h"
#include <iostream>
#include <vector>


class Set
{
	std::vector<Card> m_cardArray[3];

public:
	Set();
	void findSets() const;
	bool validateSets(const int& card1Position, const int& card2Position, const int& card3Position) const;
	~Set();
};

